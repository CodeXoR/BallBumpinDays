﻿using UnityEngine;
using UnityEditor;

public class Tools : EditorWindow {

	[MenuItem("Assets/BallBumpin'Tools/ResetHighScore")]
	static void ResetHighScore() {
		PlayerPrefs.SetFloat ("HiScore", 0f);
	}
}
