﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class VirtualJoystick : MonoBehaviour {

	public Vector2 snapBackPos = new Vector2 (50f, -50f);
	public float xBoundsMin = -25.5f;
	public float xBoundsMax = 125.5f;
	public float yBoundsMin = -125.5f;
	public float yBoundsMax = 25.5f;
	public float sensitivity = 0.5f;

	RectTransform _rectTransform = null;
	public RectTransform TransformRect {
		get { 
			_rectTransform = _rectTransform ?? GetComponent<RectTransform> ();
			return _rectTransform; 
		}
	}

	RectTransform _controlKnobRT = null;
	public RectTransform ControlKnob {
		get { 
			_controlKnobRT = _controlKnobRT ?? transform.FindChild ("ControlKnob") as RectTransform;
			return _controlKnobRT; 
		}
	}
}
