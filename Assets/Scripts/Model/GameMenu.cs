﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMenu : MonoBehaviour {

	GameObject _settingsButton = null;
	public GameObject SettingsButton {
		get {
			_settingsButton = _settingsButton ?? transform.FindChild ("SettingsButton").gameObject;
			return _settingsButton;
		}
	}

	GameObject _mainMenu = null;
	public GameObject MainMenu {
		get {
			_mainMenu = _mainMenu ?? transform.FindChild ("MainMenu").gameObject;
			return _mainMenu;
		}
	}

	Text _hiScore = null;
	public Text HiScore {
		get {
			_hiScore = _hiScore ?? MainMenu.transform.FindChild ("HiScore").FindChild("Score").GetComponent<Text> ();
			return _hiScore;
		}
	}

	Text _playButtonLabel = null;
	public Text PlayButtonLabel {
		get {
			_playButtonLabel = _playButtonLabel ?? MainMenu.transform.FindChild ("PlayButton").FindChild ("PlayButtonLabel").GetComponent<Text> ();
			return _playButtonLabel;
		}
	}
}
