﻿using UnityEngine;
using System.Collections;

public class ControlsView : Singleton<ControlsView> {

	VirtualJoystick _virtualJoystick;

	public VirtualJoystick VStick {
		get { 
			_virtualJoystick = _virtualJoystick ?? transform.FindChild ("VirtualJoystick").GetComponent<VirtualJoystick> ();
			return _virtualJoystick; 
		}
	}
}
