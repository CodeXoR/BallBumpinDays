﻿using UnityEngine;
using System.Collections;

public class PlayerController : Controller {

	Vector3 touchPos = Vector3.zero;

	VirtualJoystick vStick;
	Vector3 controlKnobPos = Vector3.zero;

	protected override void Awake () {
		base.Awake ();
		vStick = ControlsView.Instance.VStick;
	}

	protected override void Start ()
	{
		base.Start ();
		vStick.gameObject.SetActive (false);
	}

	protected override void GoBumpin ()
	{
		if (Input.touchCount > 0) {
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
				touchPos = Input.GetTouch (0).position;
				vStick.TransformRect.position = new Vector3(
					touchPos.x - (vStick.TransformRect.sizeDelta.x/2f),
					touchPos.y + (vStick.TransformRect.sizeDelta.y/2f), 
					0f);
				vStick.gameObject.SetActive (true);
			}
			if (Input.GetTouch (0).phase == TouchPhase.Moved) {
				touchPos.x += Input.GetTouch (0).deltaPosition.x;
				touchPos.y += Input.GetTouch (0).deltaPosition.y;

				float h = (Input.GetTouch (0).deltaPosition.x);
				float v = (Input.GetTouch (0).deltaPosition.y);

				controlKnobPos = vStick.ControlKnob.transform.localPosition;
				controlKnobPos += new Vector3 (h * vStick.sensitivity, v * vStick.sensitivity, 0f);

				controlKnobPos.x = controlKnobPos.x < vStick.xBoundsMin ? vStick.xBoundsMin : controlKnobPos.x;
				controlKnobPos.x = controlKnobPos.x > vStick.xBoundsMax ? vStick.xBoundsMax : controlKnobPos.x;
				controlKnobPos.y = controlKnobPos.y < vStick.yBoundsMin ? vStick.yBoundsMin : controlKnobPos.y;
				controlKnobPos.y = controlKnobPos.y > vStick.yBoundsMax ? vStick.yBoundsMax : controlKnobPos.y;
				vStick.ControlKnob.localPosition = controlKnobPos;

				body.AddTorque (((Vector3.right * v) + (Vector3.back * h)) * force, ForceMode.Acceleration);
			}
			if (Input.GetTouch (0).phase == TouchPhase.Ended) {
				vStick.ControlKnob.localPosition = vStick.snapBackPos;
				controlKnobPos = Vector3.zero;
				vStick.gameObject.SetActive (false);
			}
		}
	}
}
