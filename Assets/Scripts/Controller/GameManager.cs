﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : Singleton<GameManager> {

	bool _musicOn = true;
	public bool MusicOn {
		get { return _musicOn; }
		set { _musicOn = value; }
	}

	bool _isPaused = true;
	public bool Paused {
		get { return _isPaused; }
		set { _isPaused = value; }
	}

	bool _gameOver = false;
	public bool GameOver {
		get { return _gameOver; }
		set { 
			_gameOver = value; 
			if (_gameOver)
				StopGame ();
		}
	}

	public GameMenu gameMenu;
	Controller[] controllers = new Controller[]{};
	AudioSource audio = null;

	void Start() {
		audio = GetComponent<AudioSource> ();
		controllers = FindObjectsOfType<Controller> ();
		ToggleControllers ();
		gameMenu.HiScore.text = PlayerPrefs.GetFloat ("HiScore").ToString("N2");
	}

	void ToggleControllers() {
		foreach (Controller c in controllers) {
			c.TogglePhysics (_isPaused);
		}
		gameMenu.SettingsButton.SetActive (!_isPaused);
	}

	public void StartGame() {
		if (_gameOver) {
			RestartGame ();
		}
		_isPaused = false;
		gameMenu.PlayButtonLabel.text = "Play";
		ToggleControllers ();
		gameMenu.MainMenu.SetActive (false);
		Timer.Instance.StartTimer ();
	}

	public void StopGame() {
		_isPaused = true;
		ToggleControllers ();
		Timer.Instance.StopTimer ();
		if (_gameOver) {
			gameMenu.PlayButtonLabel.text = "Replay";
			float currentPlayTime = Timer.Instance.PlayTime;
			float hiScore = PlayerPrefs.GetFloat ("HiScore");
			if (currentPlayTime >= hiScore) {
				gameMenu.HiScore.text = currentPlayTime.ToString ("N2");
				PlayerPrefs.SetFloat ("HiScore", currentPlayTime);
			}

		} else {
			gameMenu.PlayButtonLabel.text = "Paused";
		}
		gameMenu.MainMenu.SetActive (true);
	}

	public void RestartGame() {
		foreach (Controller c in controllers) {
			c.Respawn ();
		}
		Timer.Instance.Reset ();
		_gameOver = false;
		StartGame ();
	}

	public void ToggleMusic(Text displayText) {
		_musicOn = !_musicOn;
		if (_musicOn) {
			displayText.text = "BGM: On";
			audio.UnPause ();
		} else {
			displayText.text = "BGM: Off";
			audio.Pause ();
		}
	}
}
