﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public abstract class Controller : MonoBehaviour {

	const float deathFall = -2.8f;
	bool alive = true;
	Vector3 spawnPoint = Vector3.zero;

	public float force = 28f;
	protected Rigidbody body;

	protected virtual void Awake() {
		body = GetComponent<Rigidbody> ();
	}

	protected virtual void Start() {
		spawnPoint = transform.position;
	}

	protected abstract void GoBumpin ();

	public void TogglePhysics(bool paused) {
		if (paused) {
			body.useGravity = false;
			body.velocity = Vector3.zero;
			body.angularVelocity = Vector3.zero;
		} else {
			body.useGravity = true;
		}
	}

	public void Respawn() {
		transform.position = spawnPoint;
		transform.rotation = Quaternion.identity;
	}

	void FixedUpdate() {
		if (!GameManager.Instance.Paused) {
			GoBumpin ();
			CheckGround ();
		}
	}

	void CheckGround() {
		if (transform.position.y <= deathFall) {
			alive = false;
			if (tag == "Enemy") {
				Respawn ();
			} else {
				GameManager.Instance.GameOver = true;
			}
		}
	}
}
