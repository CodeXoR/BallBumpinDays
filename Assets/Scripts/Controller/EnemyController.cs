﻿using UnityEngine;
using System.Collections;

public class EnemyController : Controller {

	/*
	 * Note:
	 * 		I was only able to implement AIClass D (dumb intelligence level)
	 * 		since I'm running out of time. -_-
	 */ 
	public enum AIClass { D, C, B, A, S }

	public AIClass aiLevel = AIClass.D;
	GameObject target = null;

	protected override void GoBumpin ()
	{
		target = target == null ? LockOnTarget () : target;
		TootsieRoll ();
	}

	/*
	 * TODO: 
	 * 		Implement varied target locking for other AIClasses C,B,A, and S.
	 * 		Implement target locking for ball bumpin battle royale (multiplayer free for all).
	 */
	GameObject LockOnTarget() {
		switch (aiLevel) {
		case AIClass.D:
		case AIClass.C:
		case AIClass.B:
		case AIClass.A:
		case AIClass.S:
		default:
			return GameObject.FindGameObjectWithTag ("Player");
			break;
		}
	}

	/*
	 * TODO:
	 *		Implement varied attacking for other AIClasses C,B,A, and S.
	 */
	void TootsieRoll() {
		switch (aiLevel) {
		case AIClass.D:
		case AIClass.C:
		case AIClass.B:
		case AIClass.A:
		case AIClass.S:
		default:
			Vector3 targetPos = target.transform.position - transform.position;
			body.AddTorque (((Vector3.right * targetPos.z) + (Vector3.back * targetPos.x)) * force);
			break;
		}
	}
}
