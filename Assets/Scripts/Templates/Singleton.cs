﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

	static T _instance = null;

	static object _lock = new object ();

	public static T Instance { 
		get { 
			_instance = FindObjectOfType<T> ();

			lock (_lock) {
				if (_instance == null) {

					GameObject singleton;
					if (typeof(T) == typeof(ControlsView)) {
						singleton = Instantiate(Resources.Load ("View/ControlsView") as GameObject);
					} else {
						singleton = new GameObject ();
					}
					singleton.name = "[Singleton] ~> " + typeof(T).ToString ();
					DontDestroyOnLoad (singleton);
					_instance = singleton.GetComponent<T> ();
					_instance = _instance ?? singleton.AddComponent<T> ();
				}
				return _instance;
			}
		} 
	}
}