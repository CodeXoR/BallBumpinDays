﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : Singleton<Timer> {

	bool recording = false;

	float _playTime = 00.00f;
	public float PlayTime {
		get { return _playTime; }
	}

	Text _playTimeDisplay = null;
	public Text PlayTimeDisplay {
		get {
			_playTimeDisplay = _playTimeDisplay ?? transform.FindChild ("Time").GetComponent<Text> ();
			return _playTimeDisplay;
		}
	}

	public void Reset() {
		_playTime = 00.00f;
		_playTimeDisplay.text = _playTime.ToString ("N2");
	}

	public void StartTimer() {
		recording = true;
	}

	public void StopTimer() {
		recording = false;
	}

	void LateUpdate() {
		if (recording) {
			_playTime += Time.deltaTime;
			PlayTimeDisplay.text = _playTime.ToString ("N2");
		}
	}
}
